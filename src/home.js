import React from "react";
import { View, Button } from "react-native";

const Home = ({navigation})=>{
    const Count = ()=> {
        navigation.navigate('countMoviment')
    }
    const Image = ()=> {
        navigation.navigate('imageMoviment')
    }
    return(
        <View>
            <Button title="Count Moviment" onPress={Count}/>
            <Button title="Image Moviment" onPress={Image}/>
        </View>
    )
}

export default Home