import React, { useRef, useState, useEffect } from "react";
import { View, Dimensions, PanResponder, Text } from "react-native";
import * as Animatable from "react-native-animatable";

const CountMoviment = () => {
  const [count, setCount] = useState(0);
  const [animationType, setAnimationType] = useState(null);
  const screenHeight = Dimensions.get("window").height;
  const gestureThreshold = screenHeight * 0.25;

  const panResponder = useRef(
    PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onPanResponderMove: (event, gestureState) => {},
      onPanResponderRelease: (event, gestureState) => {
        if (gestureState.dy < -gestureThreshold) {
          setCount((prevCount) => prevCount + 1);
          setAnimationType("bounceIn"); // Set animation type
        }
      },
    })
  ).current;

  useEffect(() => {
    if (animationType) {
      setTimeout(() => {
        setAnimationType(null);
      }, 1000); // Clear animation type after 1 second
    }
  }, [animationType]);

  return (
    <View {...panResponder.panHandlers} style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <Animatable.Text animation={animationType} style={{ fontSize: 20 }}>
        Valor do contador: {count}
      </Animatable.Text>
    </View>
  );
};

export default CountMoviment;


// "https://s2-autoesporte.glbimg.com/1AHpUbM3crKZVYtp3_iutIlwgRo=/0x0:620x300/984x0/smart/filters:strip_icc()/i.s3.glbimg.com/v1/AUTH_cf9d035bf26b4646b105bd958f32089d/internal_photos/bs/2020/G/V/YKJVbFTluO7EUtJM4vHw/2017-04-12-11-novo-dodge-demon.jpg",
// "https://cdn.motor1.com/images/mgl/JJkj4/s3/nissan-gt-r-t-spec.jpg",
// "https://cdn.motor1.com/images/mgl/xqZoAk/s1/2023-audi-r8-v10-gt-rwd.jpg",