import React, { useRef, useState } from "react";
import { View, Dimensions, PanResponder, Image, Text } from "react-native";

const ImageMoviment = () => {
    const screenWidth = Dimensions.get("window").width;
    const [direction, setDirection] = useState(0);
    const images = [
        "https://s2-autoesporte.glbimg.com/1AHpUbM3crKZVYtp3_iutIlwgRo=/0x0:620x300/984x0/smart/filters:strip_icc()/i.s3.glbimg.com/v1/AUTH_cf9d035bf26b4646b105bd958f32089d/internal_photos/bs/2020/G/V/YKJVbFTluO7EUtJM4vHw/2017-04-12-11-novo-dodge-demon.jpg",
        "https://cdn.motor1.com/images/mgl/JJkj4/s3/nissan-gt-r-t-spec.jpg",
        "https://cdn.motor1.com/images/mgl/xqZoAk/s1/2023-audi-r8-v10-gt-rwd.jpg",
    ];

    const panResponder = useRef(
        PanResponder.create({
            onStartShouldSetPanResponder: () => true,
            onPanResponderRelease: (event, gestureState) => {
                if (gestureState.dx < -50) {
                    setDirection(prevDirection => {
                        if (prevDirection === images.length - 1) {
                            return 0;
                        } else {
                            return prevDirection + 1;
                        }
                    });
                } else if (gestureState.dx > 50) {
                    setDirection(prevDirection => {
                        if (prevDirection === 0) {
                            return images.length - 1;
                        } else {
                            return prevDirection - 1;
                        }
                    });
                }
            }
        })
    ).current;

    return (
        <View {...panResponder.panHandlers} style={{justifyContent:'center', alignItems:'center',flex: 1}}>
            <Image source={{ uri: images[direction] }} style={{ width: "80%", height: 250, borderRadius:30}} />
            <Text style={{fontWeight:'bold'}}>
            Posição atual: {direction + 1}/{images.length}           
            </Text>
        </View>
    );
};

export default ImageMoviment;
