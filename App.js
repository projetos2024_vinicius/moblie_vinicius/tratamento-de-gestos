import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import Home from "./src/home"
import CountMoviment from "./src/countMoviment";
import ImageMoviment from "./src/imageMoviment";

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={Home}/>
        <Stack.Screen name="countMoviment" component={CountMoviment}/>
        <Stack.Screen name="imageMoviment" component={ImageMoviment}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}
